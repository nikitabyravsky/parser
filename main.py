import time
import os
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import urllib.request
import lxml.html as html
from time import sleep
import requests
from random import choice
from random import uniform


def get_html(url, useragent= None, proxy= None):
    print("Выполняю...")
    r = requests.get(url, headers=useragent, proxies=proxy)
    return r.text

def get_ip(html):
    soup = BeautifulSoup(html, "lxml")
    ip = soup.find("span", class_="ip").text.strip()
    ua = soup.find("span", class_="ip").find_next_sibling("span").text.strip()
    print(ip)
    print(ua)
    print("-------------------------")


def get_all_date(own_url):

    chrome_options = Options()
    chrome_options.add_argument("--headless")
    driver = webdriver.Chrome(options=chrome_options) # открытие браузера без отрисовки
    driver.get(own_url)

    photo_element = driver.find_elements_by_class_name("css-1bmvjcs") # поиск фоток объявления по классу

    i = 1
    msg = "№ фото: %s"

    try: # блок проверки на nulltype ссылки
        for elem in photo_element:
            print(msg % i)
            i += 1
            str_url = elem.get_attribute("src") #присвоение значения строке
            print(str_url) # вывод url каждой фотки
    except TypeError:
        print("Строка с NullType")
    else:
        print("...")
    finally:
        print("Good!")

    time.sleep(2)
    print('Все доступные ссылки на фото получены! \n')
    time.sleep(2)


    title = driver.find_element_by_class_name("css-r9zjja-Text") #название объявления
    adding_data = driver.find_element_by_class_name("css-19yf5ek") #дата загрузки объявления
    id_adv = driver.find_element_by_class_name("css-9xy3gn-Text") # id объявления
    desc = driver.find_element_by_class_name("css-g5mtbi-Text") #описание объявления
    price = driver.find_element_by_class_name("css-okktvh-Text") #цена объявления
    print("Название: " + title.text)
    print("Добавлено: " + adding_data.text)

    str =  id_adv.text # присвоение значения
    id = str[4:] # отображение строки, начиная с 4 элемента

    print("Id объявление: " + id)
    print("Описание: " + desc.text)
    print("Цена: " + price.text)

    create_folders(id) # создание папки с названием "id"
    j = 1
    try: # блок проверки на nulltype ссылки
        for elem in photo_element:
            string_url = elem.get_attribute("src")  # присвоение значения строке

            print("Выполняется функция загрузки фото...")
            download_photo_in_folder(id, string_url, j)
            j += 1
    except TypeError:
        print("Строка с NullType")
        os.chdir("..")  # вернуться в предыдущую директорию
        os.chdir("..")  # вернуться в предыдущую директорию
    else:
        print("Все фотки скачаны")
    finally:
        print("\n")

    write_info_in_file(id, title, adding_data, desc, price, own_url)

    driver.close()



def get_first_date(html):
    print("Получаю информацию о всех объявлениях с сайта")

    bs = BeautifulSoup(html, "lxml") # парсер в виде html документа

    all_links = bs.find_all("a", class_="marginright5") # получение всех ссылок со страницы

    for  link in all_links: #проверка ссылок
        print("----------------------------")

        a = uniform(3, 6)
        msg = "Время до открытия новой страницы: %s \n"
        print(msg % a)
        time.sleep(a)

        print(link["href"])

        own_url = link["href"] #ссылка для детального парсинга

        own_link = link["href"] #ссылка на проверку, есть ли объявление в базе

        ch = check_link(own_link) # присвоение булевого значения из цикла


        if ch == False: # если объявления НЕТ в базе, идет парсинг
            print("Ссылки в базе нет, получаю данные об объявлении \n")
            time.sleep(a)
            get_all_date(own_url)
        else:
            print("Найдено совпадение с ссылкой, выполянется перебор следующей \n")

def check_link(own_link):
    print("Проверяю, есть ли ссылка УЖЕ в базе... \n")

    check = False # булева = 0

    f = open("orders_link.txt") # открытие файла с ссылками
    if own_link in f.read(): # проверка на наличие ссылки в файле
        check = True # если есть ссылка, то булева = 1
        f.close() # файл закрывается, парсинг не идет
    else:
        f = open("orders_link.txt", "a") # если нет ссылки, то открываем для записи в конец файла
        f.write(own_link + "\n") # запись ссылки с отступом на новую строку
        f.close()

    return check # возврат булевы


def create_folders(folder_name):
    os.chdir("orders") # открыть папку заказы
    print("Создал новую папку для объявления")
    os.mkdir(folder_name)
    os.chdir("..") # ернуться в исходную директорию

def write_info_in_file(folder_name, title, adding_date, desc, price, url):
    os.chdir("orders")  # открыть папку заказы
    os.chdir(folder_name)  # открыть папку заказы
    print("Создаю файл для информации об объявлении")
    name = str(folder_name)
    file_format = ".txt"
    file_name = name + "_info" + file_format
    info_file = open(file_name, "w", encoding='utf-8')
    title_str = title.text
    adding_date_str = adding_date.text
    desc_str = desc.text
    price_str = price.text
    info_file.write("----------------------- \n")
    info_file.write("НАЗВАНИЕ \n")
    info_file.write(title_str + "\n")
    info_file.write("----------------------- \n")
    info_file.write("ССЫЛКА \n")
    info_file.write(url + "\n")
    info_file.write("----------------------- \n")
    info_file.write("ID объявления \n")
    info_file.write(name + "\n")
    info_file.write("----------------------- \n")
    info_file.write("ДАТА ДОБАВЛЕНИЯ \n")
    info_file.write(adding_date_str + "\n")
    info_file.write("----------------------- \n")
    info_file.write("ОПИСАНИЕ \n")
    info_file.write(desc_str + "\n")
    info_file.write("----------------------- \n")
    info_file.write("ЦЕНА \n")
    info_file.write(price_str + "\n")
    info_file.write("----------------------- \n")

    info_file.close()
    print("Информация записана в файл")

    try:
        f = open('4.png')
        f.close()
        os.remove("4.png")
        print("Отлично, все файлы успешно загружены!")
    except FileNotFoundError:
        print("Отлично, все файлы успешно загружены!")


    os.chdir("..")# вернуться в предыдущую директорию
    os.chdir("..")# вернуться в предыдущую директорию



def download_photo_in_folder(folder_name, url, name_photo):
    name = str(name_photo)
    format = ".png"
    file_name = name+format
    os.chdir("orders")  # открыть папку заказы
    os.chdir(folder_name)  # открыть папку заказы
    photo_file = open(file_name, "wb") #создается файл фотки для записи
    print("Файл для фото успешно создан!")

    photo_url = url

    urllib.request.urlretrieve(photo_url, '/Users/Byrevestnyii/PycharmProjects/parser/orders/' +folder_name + '/' + file_name)
    print("Фото успешно сохранено! \n")
    photo_file.close()

    os.chdir("..")# вернуться в предыдущую директорию
    os.chdir("..")# вернуться в предыдущую директорию

def main():
    print("Hello, NIKITA!")

    url = "https://www.olx.pl/elektronika/telefony/gdansk/q-Iphone/"

    user_agents = open("user_agents.txt").read().split("\n")
    proxies = open("ip.txt").read().split("\n")

    a = uniform(3, 6)
    print(a)
    sleep(a)

    proxy = {"http": "http://" + choice(proxies)}
    useragent = {"User-Agent": choice(user_agents)}

    html = get_html(url, useragent, proxy)
    get_first_date(html)

main()